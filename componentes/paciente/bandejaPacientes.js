//Controlador página inicial
app.controller("controladorBandejaPacientes", ['$scope','fShared','$http','urls','mensajesFlash', '$filter', function($scope,fShared,$http,urls,mensajesFlash,$filter){
	console.log("bandejaPacientes.tmpl.html  bandejaPacientes.js");

	

		$scope.tableInfo = 
    			[{title:"Nombre", key:"nombre"},
    			 {title:"Apellido Paterno", 	key:"apellidoPaterno"},
    			 {title:"Apellido Materno", 	key:"apellidoMaterno"},
    			 {title:"Estado Civil", 		key:"estadoCivil"},
    			 {title:"Edad", 		key:"edad"}];


	consultarPacientes();
	function consultarPacientes() {
	    console.log("entro consultarPacientes");  
	     var medico = angular.toJson({
                      "idMedico": 1
                     
                    });
	    $scope.loading = true;
	    $http({
	                url: urls.bandejaPacientes + 'bandejaPacientes.php',
	                method: "POST",
	              //  data:  $.param({usuario:usuario.email, password:passwordEnc}),
	                data:  medico,
	                headers: {'Content-Type': 'application/json'}
	                //    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
	            }).success(function(data){
	                console.log("pacientes: ",data);
	               $scope.pacientes = data;
	               if($scope.iniciarTabla != undefined) $scope.iniciarTabla(data);
	                 $scope.loading = false;
	    
	            }).error(function(data,status){
	            	$scope.loading = false;
	                if(status==400){
	                    mensajesFlash.show(data.mensaje,"danger");
	                }
	                if(status ==404){
	                	
	                	mensajesFlash.show("El host ha sido capaz de comunicarse con el servidor, pero no existe el recurso que ha sido pedido.","danger");
	                }
	            })
	            // The function returns the product of p1 and p2
}


   
  
}]);

