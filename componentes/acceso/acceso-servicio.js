app.factory("accesoServicio", ['$http', '$location', 'sesionesControl', 'mensajesFlash', 'urls', 'fShared', function($http, $location, sesionesControl, mensajesFlash, urls, fShared)
{
    var cacheSession = function(datos){
        sesionesControl.set("usuarioLogin", true);
       /* var usuario ={
          nombre: datos.usuario.nombre,
          color: datos.usuario.color
        };*/
        sesionesControl.setUserDoctor("userDoctor",datos);
        //sesionesControl.setList("usuario",usuario);
    }
    var unCacheSession = function(){
        sesionesControl.unset("usuarioLogin");
        sesionesControl.unset("usuario");
    }

    return{
        login : function(usuario){
            console.log("Contraseña antes de desifrar: ",usuario.password);
            var passwordEnc = "" + CryptoJS.SHA512(usuario.password);
            console.log("Contraseña despues de desifrar: ",passwordEnc);
            var user3 = angular.toJson({
                      "usuario": usuario.email,
                      "password":usuario.password
                     
                    });
            return $http({
                url: urls.loginUsuario + 'login.php',
                method: "POST",
              //  data:  $.param({usuario:usuario.email, password:passwordEnc}),
                data:  user3,
                headers: {'Content-Type': 'application/json'}
                //    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(data){
                console.log("Info Login: ",data)
                mensajesFlash.clear();
                cacheSession(data);
                sesionesControl.unset("mensaje");
                 //fShared.setDetalleTesoreria(5);
                $location.path('/inicio');
            }).error(function(data,status){
                if(status==400){
                    mensajesFlash.show(data.mensaje,"danger");
                }
            })
        },
        logout : function()
        {
            sesionesControl.clear();
            sesionesControl.setList("mensaje", {texto:"¡Hasta pronto!", tipo:"success"});
            $location.path("/login");
        }
    }
}]); 
