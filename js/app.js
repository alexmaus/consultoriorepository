var app = angular.module('miProyecto', ['ngRoute', 'ui.bootstrap', "MainFilter"]);

// constantes Alex
app.constant("urls", {
    "servidor" : "http://localhost/angularServidor/",
    "loginUsuario" : "http://localhost/consultorioServices/consultaDB/",
    "bandejaPacientes" : "http://localhost/consultorioServices/consultaDB/bandejasDB/"
});
