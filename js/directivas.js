app.directive('tooltip', function() {
  return {
    restrict: 'EA',
    link: function(scope, element, attrs) {
      attrs.tooltipTrigger = attrs.tooltipTrigger;
      attrs.tooltipPlacement = attrs.tooltipPlacement || 'top';
    }
  }
});

app.directive('gridGenerico',
		function($filter) {
			return {
				templateUrl : "componentes/gridGenerico/grid-generico.tmpl.html",

				restrict : 'AE',
				scope : {
					init : "=",
					data : "=",
					columnsInfo : "=",
					selectedRow : "=",
					showDetails : "=",
					downloadFile : "=",
					uploadFile : "=",
					color : "=",
					showReport : "=",
					presonalizedData : "=",
					lock : "=",
					unlock : "=",
					cancel : "=",
					detail : "=",
					binnacle : "=",
					updateSaldos : "=",
					showInstruccion : "=",
				},
				link : function(scope, element, attr) {

					scope.filterBar = true;
					if (attr.filterBar === "false")
						scope.filterBar = false;

					scope.pagination = true;
					if (attr.pagination === "false")
						scope.pagination = false;

					// *****Metodo que inicializa la tabla********
					scope.init = function(data) {
						console.log("Directive :: grid-generico");
						scope.data = data;
						scope.infoFiltrar = "";// Se elimina el filtro
						// actual

						scope.valor = undefined;
						scope.orden = false;
						scope.pagMax = 1;
						scope.pagActual = 1;

						scope.filteredItems = scope.data;
						if (scope.filteredItems.length > 10)
							scope.itemsPerPage = 10;
						else
							scope.itemsPerPage = scope.filteredItems.length;

						scope.groupToPages();
					};
					function resetSelected() {
						scope.selected = undefined;
						scope.selectedRow = {};
					}
					;

					// Agrupa los datos en pagina
					scope.groupToPages = function() {
						if (scope.itemsPerPage <= 0)
							scope.itemsPerPage = 1;
						else if (scope.itemsPerPage > scope.filteredItems.length
								|| isNaN(scope.itemsPerPage))
							scope.itemsPerPage = scope.filteredItems.length;

						scope.pagedItems = [];
						scope.pagActual = 1;
						for (var i = 0; i < scope.filteredItems.length; i++) {
							if (i % scope.itemsPerPage === 0) {
								scope.pagedItems[Math.floor(i
										/ scope.itemsPerPage)] = [ scope.filteredItems[i] ];
								scope.pagMax = Math.floor(i
										/ scope.itemsPerPage) + 1;
							} else {
								scope.pagedItems[Math.floor(i
										/ scope.itemsPerPage)]
								.push(scope.filteredItems[i]);
							}
						}
						resetSelected();
					};

					/** **************Metodos de Paginacion************* */
					scope.first = function() {// Ir a primera pagina
						if (scope.pagActual !== 1) {
							scope.pagActual = 1;
							resetSelected();
						}
					};
					scope.prev = function() {// Ir a pagina anterior
						if (scope.pagActual > 1) {
							scope.pagActual--;
							resetSelected();
						}
					};
					scope.next = function() {// Ir a siguiente pagina
						if (scope.pagActual < scope.pagMax) {
							scope.pagActual++;
							resetSelected();
						}
					};
					scope.last = function() {// Ir a ultima pagina
						if (scope.pagActual !== scope.pagMax) {
							scope.pagActual = scope.pagMax;
							resetSelected();
						}
					};
					/** ************Metodo De Filtrado***************** */
					scope.filtrar = function() {
						if (scope.infoFiltrar.length > 2)
							scope.filteredItems = $filter('filter')(
									scope.data, scope.infoFiltrar);
						else
							scope.filteredItems = scope.data;

						if (scope.filteredItems.length > 10)
							scope.itemsPerPage = 10;
						else
							scope.itemsPerPage = scope.filteredItems.length;

						scope.groupToPages();
					}
					/** ***Metodo para ordenar los datos por columna**** */
					scope.sort = function(newValor) {
						if (scope.valor === newValor) {
							scope.orden = !scope.orden;
						}
						scope.valor = newValor;
					};
					/* Funcion para establecer el registro seleccionado */
					scope.setSelected = function(selected, selectedRow) {
						if (scope.selected != selected) {

							scope.selected = selected;
							scope.selectedRow = selectedRow;
						}
					};
					// Este metodo se llama en el caso en que la
					// directiva inicia despues
					// de que se hace la consulta de los datos en el
					// controller y se llama el metodo
					// init desde fuera
					if (scope.data != undefined) {
						scope.init(scope.data);
					}
				}

			}
		});